# MLOps on ods.ai. Coursework

link: https://ods.ai/tracks/ml-in-production-spring-22

## Environment

```bash
Create env:
$ python3 -m venv env

Activate env:
$ source env/bin/activate

Exit env:
$ deactivate
```

## Dependencies

```bash
$ pip3 install -r requirements.txt
```
